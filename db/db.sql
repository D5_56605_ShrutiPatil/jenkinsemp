create table Emp(
    empid int,
    name varchar(20),
    salary double,
    age int
);

insert into Emp values(1, "harry", 5000, 17);
insert into Emp values(2, "ron", 4000, 18);

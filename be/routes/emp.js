const express= require('express')
const db =require('../db')
const utils =require('../utils')
const router =express.Router()

router.get('/getallemps', (request, response)=>
{
    const statement =`select * from Emp`
    const connection= db.openConnection()
    connection.query(statement, (error, result) =>{
        connection.end()
        response.send(utils.createResult(error, result))
    })
})

router.post('/addemp',(request, response)=>
{
    const{empid, name, salary, age }= request.body
    const statement=
    `insert into Emp
    (empid, name, salary, age)
    values
    ('${empid}', '${name}', '${salary}', '${age}')`

    const connection= db.openConnection()
    connection.query(statement, (error, result) =>{
        connection.end()
        response.send(utils.createResult(error, result))
    })
})
router.delete('/delete',(request, response)=>
{
    const{empid}= request.body
    const statement=
    `delete from Emp where empid= ${empid}`
    const connection= db.openConnection()
    connection.query(statement, (error, result) =>{
        connection.end()
        response.send(utils.createResult(error, result))
    })
})

router.put('/update', (request, response) => {
    const{empid, salary}=request.body
    const statement = `update Emp set salary= '${salary}' where empid = '${empid}' `
  
    const connection = db.openConnection()
    connection.query(statement, (error, result) => {
      connection.end()
      response.send(utils.createResult(error, result))
    })
  })


module.exports = router